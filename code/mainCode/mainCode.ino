#include <LiquidCrystal.h>

/*
  pin no.  lcd name    ard pin                                 Potentiometer Illustration wiring
  16       K           GND                                                  V0
  15       A           5V                                                   |
  14       D7          12                                                   |
  13       D6          11                                                   0
  12       D5          10                                                  0o0                
  11       D4          9                                                  / 0 \
  6        E           8                                                 /     \
  5        RW          GND                                              5V      GND
  4        RS          7
  3        V0          potentiometer pin
  2        VDD         5V
  1        VSS         GND

*/


#define checkInterval 3                 // interval in seconds to update speed
#define wheelRadius 10                  // 10 cm radius of wheel
#define sensorPin 13                    // pin where input is read from


// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(7, 8, 9, 10, 11, 12);

unsigned long revolutions;                // total number of revolutions since startup
double totalDistanceCm;                    // total distance in CM
float speedKmH;                            //speed

unsigned short int revFlagA;              //flag to check that a revolution is count once
unsigned short int revFlagB;              // debounce like above

unsigned short int sensorState;          // stateflag

unsigned long startTimeFlag;            // time since speed calculation started
unsigned int endTimeScale;              // time until speed will be measured
unsigned short int tmpRevolutions;      // tmp resolutions that were done between the above times

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("startup...");
  
  revolutions = 0;                      // init revolutions
  sensorState = digitalRead(sensorPin);  // set hall sensor as digital input pin
  totalDistanceCm = 0;                  // init distance in cm
  speedKmH = 0;                        // init speed
  
  endTimeScale = checkInterval *1000;          //speed is updated every 3seconds
  startTimeFlag = millis();    // start time is set from where the code started running
  tmpRevolutions = 0;          // tmp revolutions init
}

void loop() {  
  // put your main code here, to run repeatedly:
  revFlagA = digitalRead(sensorPin);    // check pin val
  delay(10);                            // 10 ms delay
  revFlagB = digitalRead(sensorPin);    // check again pin val
  
  if(revFlagA == revFlagB){
    if(revFlagA != sensorState){
      if(revFlagA == HIGH){
        revolutions++;      // used to store total number of revolutions
        tmpRevolutions++;  //used to calculate speed
        
        Serial.print("Revolutions:");
        Serial.println(revolutions);
        Serial.print("Aprox. Dist.:");
        calculateDistance();
      }
    }
    sensorState = revFlagA;
  }
  calculateSpeed();
}

void calculateDistance(){
  totalDistanceCm = revolutions*(2*wheelRadius*PI);
  
  lcd.setCursor(0, 1);
  lcd.print("Dist:");
  
  if(totalDistanceCm < 100000){ 
    
    lcd.setCursor(5, 1); 
    lcd.print(totalDistanceCm/100);
    lcd.setCursor(10, 1); 
    lcd.print("m");
    
    Serial.print(totalDistanceCm/100); //under 1km (100000cm)
    Serial.println("m");
  }else{
    
    lcd.setCursor(5, 1); 
    lcd.print((totalDistanceCm/100)/1000);
    lcd.setCursor(10, 1); 
    lcd.print("km");
    
    Serial.print((totalDistanceCm/100)/1000); // over 1km
    Serial.println("km");
  }
}

void calculateSpeed(){
  if(millis() > (startTimeFlag + endTimeScale)){    // if current time is bigger than start time + end time interval
    startTimeFlag = millis();                        // start time is set to current time
    
    float tmpDistance = tmpRevolutions*(2*wheelRadius*PI);          // distance between start time and end time is calculated
    
    speedKmH = (tmpDistance/checkInterval)*0.036;    // 0.036 used to convert to km/h in order to return speed
   
    lcd.setCursor(0, 0);
    lcd.print("Speed:");
    lcd.setCursor(6, 0); 
    lcd.print(speedKmH);
    lcd.setCursor(10, 0); 
    lcd.print("km/h");
    
    Serial.print("Speed: ");      // print
    Serial.print(speedKmH);       // speed
    Serial.println("km/h");
    tmpRevolutions = 0;          // revolutions are set back to 0
  }
}

