# README #

Just follow the schema for wiring and upload the code to your arduino.

DEMO VIDEO: https://www.youtube.com/watch?v=h-9G_S2CQkM


### What is this repository for? ###

* Speedometer source.
* v0.1
* [Link to repo](https://bitbucket.org/crisonescu/arduino-speedometer)

### How do I get set up? ###

* Things you need: arduino
* magnetic hall sensor
* 16x2 lcd display
* power bank
* 5v to 3.3v voltage regulator
* follow my steps

### Contribution guidelines ###

* XXXXXXX

### Who do I talk to? ###

* Repo owner or admin